//Program to do different tasks

void drive(long nMotorRatio, long dist, long pwr);
void turn90(long nMotorRatio, long pwr);

task main()
{
	long nMotorRatio = 0;
	long dist = 10000;
	long pwr = 35;

	drive(nMotorRatio, dist, pwr);

	turn90(nMotorRatio, pwr);
}

//Function to test 'setMotorSyncEncoder' by driving forward for 3 seconds.
//Part 2 adds driving in a square of random direction.
void drive(long nMotorRatio, long dist, long pwr)
{
	int randDirection = 0;
	int i = 0;

	randDirection = random(1);

	//go in a square of perimeter of 200 cm
	if (randDirection ==  0)
	{
		while (i != 4)
		{
			setMotorSyncEncoder(motorB, motorC, 100, dist, -50);
			sleep(450);
			
			setMotorSyncEncoder(motorB, motorC, nMotorRatio, dist, pwr);
			sleep(3000);

			i++;
		}
	}

	else
	{
		while(i != 4)
		{
			setMotorSyncEncoder(motorB, motorC, 100, dist, -50);
			sleep(450);
			
			setMotorSyncEncoder(motorB, motorC, nMotorRatio, dist, pwr);
			sleep(3000);

			i++;
		}
	}
}

//Function to test 'setMotorSyncEncoder'
void turn90(long nMotorRatio, long pwr)
{
	//drive forward at full power for 100cm
	setMotorSyncEncoder(motorB, motorC, nMotorRatio, 8000, 100);
	sleep(4000);

	//turn 180 degrees and return to original spot at 25% power
	setMotorSyncEncoder(motorB, motorC, 100, 10000, -50);
	sleep(850);

	setMotorSyncEncoder(motorB, motorC, nMotorRatio, 10000, pwr);
	sleep(10000);
}




