#pragma config(StandardModel, "EV3_REMBOT")

int getDistance(int distance) 
{
	
	// Loop until button pressed
	while (getButtonPress(buttonEnter) == 0 )  // this will fail once the button is enter pressed. 
	{
		
		displayCenteredBigTextLine(4,"Distance = %dcm", distance);
	
		if (getButtonPress(buttonUp)) {
			distance = 10;
			displayCenteredBigTextLine(4,"Distance = %dcm", distance);
		}
		else if (getButtonPress(buttonRight)) {
			distance = 40;
			displayCenteredBigTextLine(4,"Distance = %dcm", distance);
		}

        else if (getButtonPress(buttonDown)) {
			distance = 60;
			displayCenteredBigTextLine(4,"Distance = %dcm", distance);
		}

        else if (getButtonPress(buttonLeft)) {
			distance = 80;
			displayCenteredBigTextLine(4,"Distance = %dcm", distance);
		}
		
		// Wait 20 ms, this gives us 50 readings per second
		sleep(20);
	}
	return distance;
	
}


task main()
{
    //433.333333333=20cm approx
    //cm to distance factor=21.6666666667
	// Use this if you want to use the back button as well
	// Not advisable.
	// setBlockBackButton(true);
	
	int distance = 0;
	int movement_speed=random(100);
    int cm_to_distance=21.6666666667;
	
	displayCenteredTextLine(1, "Pressed button:");
	distance=getDistance(distance);
	displayCenteredBigTextLine(6,"Final Dist = %dcm", getDistance(distance));
	setMotorSyncEncoder(motorB, motorC, 0, distance*cm_to_distance, movement_speed);
	sleep(2000);
	
}