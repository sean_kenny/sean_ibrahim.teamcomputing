void turn90degreesLeft();
void turn90degreesRight();
void goForward1second();
void swingRight90degrees();
void swingLeft90degrees();
void reverse1second();


task main()
{
	turn90degreesLeft();
	turn90degreesRight();
	goForward1second();
	swingRight90degrees();
	swingLeft90degrees();
	reverse1second();
}


void turn90degreesLeft()
{
	setMotorSpeed(motorB, -50);
	setMotorSpeed(motorC, 50);
	sleep(375);
}

void turn90degreesRight()
{
	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, -50);
	sleep(375);
}

void goForward1second()
{
	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, 50);
	sleep(1000);
}

void swingRight90degrees()
{
	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, 0);
	sleep(700);
}

void swingLeft90degrees()
{
	setMotorSpeed(motorC, 50);
	setMotorSpeed(motorB, 0);
	sleep(700);
}

void reverse1second()
{
	setMotorSpeed(motorB, -50);
	setMotorSpeed(motorC, -50);
	sleep(1000);
}

