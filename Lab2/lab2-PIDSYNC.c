/*Program meant for robot on carpet, not simulator*/

void moveRobot(long nMotorRatio, long time, long pwr);
void randomDirection();

task main()
{
	long nMotorRatio = 100;
	long time = 1000;
	long pwr = 50;

	//call function to test PID
	moveRobot(nMotorRatio, time, pwr);

	//call function to return a random number from 0-1
	randomDirection();

}//end main


void moveRobot(long nMotorRatio, long time, long pwr)
{
	int count = 0;

	//turn 90 degrees left
	setMotorSyncTime(motorB, motorC, 100,  1000, 25);
	sleep(1500);

	//turn 90 degrees right
	setMotorSyncTime(motorB, motorC, -100, 1000, 25);
	sleep(1500);

	while(count !=4)
	{
		// Move forward at half power for 1 metre
		setMotorSpeed(motorB, 50);	//Set the leftMotor (motor1) to half power forward (50)
		setMotorSpeed(motorC, 50); 	//Set the rightMotor (motor6) to half power forward (50)
		sleep(2900);			//Wait for 1.5 seconds before continuing on in the program.

		// turn 90 degrees
		setMotorSpeed(motorB, 50);
		setMotorSpeed(motorC, -50);
		sleep(357);

		count++;
	}

	//go forward at full speed for 2 seconds
	setMotorSpeed(motorB, 100);
	setMotorSpeed(motorC, 100);
	sleep(2000);

	//turn 180 degrees
	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, -50);
	sleep(714);

	//go forward at half speed for 4 seconds
	setMotorSpeed(motorB, 50);
	setMotorSpeed(motorC, 50);
	sleep(4000);
}//end moveRobot


void randomDirection()
{
	//get a random number
	int randm = random(1);

		//turn in a certain direction, depending on returned value
	if (randm == 1)
	{
		setMotorSyncTime(motorB, motorC, 100,  1000, 50);
		sleep(1500);
	}//end if

	else
	{
		setMotorSyncTime(motorB, motorC, -100, 1000, 50);
		sleep(1500);
	}//end else
}//end randomDirection
